import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import List from '../page/List';
import Header from './Header';
import Layout from './Layout';

const Home = () => {
    const [item, setItem] = useState([]);
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();

    const handle = () => {
        setLoading(true);
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then((res => setItem(res.data)))
            
            .catch(err => console.log(err));
            setLoading(false)
    };

    useEffect(() => {
        // handle();
    }, []);
    const titleClicked = (id) => {
        return navigate(`/home/${id}`);
    };

    return (
        <Layout>

            {loading ? '' : (<button onClick={handle}> fetch</button>)}
            {loading ? (<h1>{console.log('loading..')} loading...</h1>) : (
                item.map((data, key) => {
                    return (
                        <h1 key={data.id}>{data.body} </h1>
                    );
                })
            )}

        </Layout>
    );

};
export default Home;