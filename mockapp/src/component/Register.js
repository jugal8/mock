import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { useNavigate } from 'react-router-dom';
const SignupSchema = Yup.object().shape({
    password: Yup.string()
        .min(7, 'Too Short!')
        .max(12, 'Too Long!')
        .required('Required'),
    email: Yup.string().email('Invalid email').required('Required'),
});

const Register = () => {

    const navigate = useNavigate();
    return (
        <div>
            <h1>Login</h1>
            <Formik
                initialValues={{
                    password: '',
                    email: '',
                }}
                validationSchema={SignupSchema}
                onSubmit={values => {
                    console.log(values);
                    navigate("/home");

                }}
            >
                {({ errors, touched }) => (
                    <Form>
                        <Field name="email" type="email" placeholder='Email' autoComplete="off" />
                        {errors.email && touched.email ? <div>{errors.email}</div> : null}

                        <Field name="password" type='password' placeholder='Password' autoComplete="off" />
                        {errors.password && touched.password ? (
                            <div>{errors.password}</div>
                        ) : null}
                        {/* {!(!isValid || (Object.keys(touched).length === 0 && touched.constructor === Object)) ? <Link to='/home'>login</Link>: */}
                        <button>login</button>

                    </Form>
                )}
            </Formik>
        </div>
    );
};

export default Register;