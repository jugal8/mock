
// import React, { useState } from 'react';
// import { Formik, Form, Field } from 'formik';
// import * as Yup from 'yup';
// import axios from 'axios';

// const SignupSchema = Yup.object().shape({
//     password: Yup.string()

//         .required('Required'),

//     email: Yup.string().email('Invalid email').required('Required'),
// });

// const Login = () => {
//     // const [data, setData] = useState({
//     //     email: '',
//     //     password: ''
//     // });

//     const [isValid, setIsValid] = useState('');



//     const clickHandler = () => {
//         const value = axios.get('https://react-http-62968-default-rtdb.firebaseio.com/register.json')
//             .then((res => { console.log(res); })).catch(ERR => { console.log(ERR); });

//         // if (value === )

//     };
//     return (
// <>
//         <div>
//             <h1>Login</h1>
//             <Formik
//                 initialValues={{
//                     password: '',

//                     email: '',
//                 }}
//                 validationSchema={SignupSchema}
//                 onSubmit={values => {


//                 }}
//             >
//                 {({ errors, touched }) => (
//                     <Form>
//                         <Field name="email" type="email" placeholder='Email' autoComplete="off" onChange />
//                         {errors.email && touched.email ? <div>{errors.email}</div> : null}


//                         <Field name="password" type='password' placeholder='Password' autoComplete="off" />
//                         {errors.password && touched.password ? (
//                             <div>{errors.password}</div>
//                         ) : null}
//                         <button > Sign Up</button>
//                         <button type="submit" onClick={clickHandler}>Login</button>
//                     </Form>
//                 )}
//             </Formik>
//         </div>

//         </> );
// };

// export default Login;

import axios from 'axios';
import React, { useEffect, useState } from 'react';

const Login = () => {
    const [data, setData] = useState(null);

    const clickHandler = () => {
        axios.get('https://react-http-62968-default-rtdb.firebaseio.com/register.json')
            .then(res => setData(res.data));
    };


    // useEffect(()=>{
    //     if(data) {
    //         const loadedMovies = [];
    //         console.log(data)
    //         for (const key in data) {
    //             console.log(key)
    //             loadedMovies.push({
    //                 id: key,
    //                 email: data[key].values.email,
    //             });
    //         }
    //         setNewdata(loadedMovies);
    //     }
    // }, [data])

    if(data) {
        console.log(Object.entries(data))
    }

    return (<>
        <div>

            <button className='btn btn-primary' onClick={clickHandler}>Fetch</button>
            <ul>
                {data && Object.entries(data).map(([key, value]) => 
                    (<div key={key}>{value.values.email}</div>)
                )}
                

            </ul>
        </div>
    </>);
};

export default Login;





