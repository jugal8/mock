import './App.css';
import Home from './component/Home';
import Login from './component/Login';
import Register from './component/Register';
import { Routes, Route } from 'react-router-dom';
import About from './page/About';
import Career from './page/Career';
import List from './page/List';
function App() {
  return (
    <div className="App">


      <hr />
      {/* <Login/> */}

      <Routes>
        <Route path='/' element={<Register />} />
        <Route path='/home' element={<Home />} >
          <Route path='/home/:id' element={<List />} />
        </Route>

        <Route path='/about' element={<About />} />
        <Route path='/career' element={<Career />} />
      </Routes>
    </div>
  );
}

export default App;
 //